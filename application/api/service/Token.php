<?php
/**
 * Created by PhpStorm.
 * User: Knight
 * Date: 2019/1/30
 * Time: 13:58
 */

namespace app\api\service;


use app\api\exception\ParameterException;
use app\api\exception\UserException;
use app\api\model\WxUser as WxUserModel;
use app\api\util\JwtUtil;
use think\Exception;
use think\Request;

class Token
{

    /**
     * 验证token中的unionid是否合法
     */
    public static function tokenValidateScope()
    {

        $token=Request::instance()->header('token');
        if (!$token) {
            throw new ParameterException();
        }
        //jwt解密
        try {
            $tokenarr = JwtUtil::jwtDecode($token);
            $unionid = $tokenarr->{'unionid'};
        } catch (Exception $e) {
            throw new ParameterException();
        }

        $wxUserModel=new WxUserModel();
        $num=$wxUserModel->where('unionid',$unionid)->count();
        if(!$num){
            throw new UserException(['msg' => 'token参数错误', 'errorCode' => '30001']);
        }



    }

}