<?php
/**
 * Created by PhpStorm.
 * User: Knight
 * Date: 2018/10/23
 * Time: 16:41
 */

namespace app\api\controller;



use app\api\exception\ParameterException;
use app\api\util\JwtUtil;
use app\api\validate\WxValidate;
use app\api\service\Index as IndexService;
use app\api\service\User as UserService;
use app\api\service\PickStar as PickStarService;
use think\Exception;

class Index extends BaseController
{
    protected $beforeActionList=[
//        'tokenValidateScope' => ['only' => 'getRunData']//校验登录状态
    ];

    /**
     * 1.通过code换取openid及session_key并返回
     * @return \think\response\Json
     */
    public function codeToOpneid(){
        (new WxValidate())->goCheck('index');//验证参数code有效性
        $code=request()->param('code');

        //返回sessen_key和openid
        $indexService=new IndexService();
        $arr=$indexService->codeToOpneid($code);
        return json($arr,200,$this->setHeader());
    }

    /**
     * 2.通过encryptedData,iv,openid,session_key解密获取unionid
     */
    public function getToken(){
        (new WxValidate())->goCheck('gettoken');//验证参数有效性
        $params=request()->post();
         
        //返回sessen_key和openid
        $indexService=new IndexService();
        $arr=$indexService->getToken($params);
        return json($arr,200,$this->setHeader());
    }







}