<?php
/**
 * Created by PhpStorm.
 * User: Knight
 * Date: 2019/1/30
 * Time: 19:24
 */

namespace app\api\controller;

use app\api\exception\ParameterException;
use app\api\service\Prize as PrizeService;
use app\api\util\JwtUtil;
use think\Exception;

class Prize extends BaseController
{

    protected $beforeActionList=[
        'tokenValidateScope' => ['only' => 'draw,checkprize,getprize']//校验登录状态
    ];

    /**
     * 获取奖品信息
     */
    public function getPrizeList(){
        $prizeService=new PrizeService();
        $data=$prizeService->getPrizeList();
        return json($data,200,$this->setHeader());
    }

    /**
     * 用户抽奖
     */
    public function draw(){
        $token=request()->header('token');
        //jwt解密
        try {
            $tokenarr = JwtUtil::jwtDecode($token);
            $unionid = $tokenarr->{'unionid'};
        } catch (Exception $e) {
            throw new ParameterException();
        }

        $prizeService=new PrizeService();
        $data=$prizeService->draw($unionid);
        return json($data,200,$this->setHeader());

    }


    /**
     * 用户抽奖记录查询
     */
    public function checkprize(){
        $token=request()->header('token');
        //jwt解密
        try {
            $tokenarr = JwtUtil::jwtDecode($token);
            $unionid = $tokenarr->{'unionid'};
        } catch (Exception $e) {
            throw new ParameterException();
        }
        $prizeService=new PrizeService();
        $data=$prizeService->checkprize($unionid);
        return json($data,200,$this->setHeader());
    }

    /**
     * 工作人员领取奖品
     */
    public function getUserprize(){
        $token=request()->header('token');
        $prizeid=request()->post('prizeid');
        //jwt解密
        try {
            $tokenarr = JwtUtil::jwtDecode($token);
            $unionid = $tokenarr->{'unionid'};
        } catch (Exception $e) {
            throw new ParameterException();
        }
        $prizeService=new PrizeService();
        $data=$prizeService->getprize($unionid,$prizeid);
        return json($data,200,$this->setHeader());
    }



}