<?php
/**
 * Created by PhpStorm.
 * User: Knight
 * Date: 2019/1/30
 * Time: 13:57
 */

namespace app\api\controller;
use app\api\model\ZdIndexButton as ZdIndexButtonModel;
use app\api\model\nav_index\NavigationFirst as ZdNavigationIndexModel;

/**
 * 首页模块按钮
 * Class IndexButton
 * @package app\api\controller
 */
class IndexButton extends BaseController
{
    /**
     * 1.获取板块入口数据
     */
    public function getButton(){
        $zdIndexButtonModel=new ZdIndexButtonModel();
        $list=$zdIndexButtonModel->where('status',0)
            ->order('weight','desc')
            ->field('title,img,jump_type,appid,url,weight')
            ->select();
        return json(['code'=>1,'data'=>$list],200,$this->setHeader());
    }

    /**
     * 2.获取首页底部导航
     */
    public function getBottomButton(){
        $zdNavigationIndexModel=new ZdNavigationIndexModel();
        $nav = $zdNavigationIndexModel->with(['navigation' => function ($query) {
            $query->where('status', 0)
                ->field('title,jump_type as jumpType,appid,url,weight,pid')
                ->order('weight desc');
        }])
            ->where([
                'status' => 0,
                'pid' => 0,
            ])
            ->field('title,jump_type as jumpType,appid,url,weight,id')
            ->order('weight desc')
            ->select();
        return json(['code'=>1,'data'=>$nav],200,$this->setHeader());;

    }

}