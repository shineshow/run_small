<?php
/**
 * Created by PhpStorm.
 * User: Knight
 * Date: 2019/1/30
 * Time: 16:48
 */

namespace app\api\controller;


use app\api\exception\ParameterException;
use app\api\exception\StepException;
use app\api\model\UserStepRecord;
use app\api\util\JwtUtil;
use app\api\validate\WxValidate;
use app\api\service\Step as StepService;
use Exception;
use think\Log;

class Step extends BaseController
{
    protected $beforeActionList=[
        'tokenValidateScope' => ['only' => 'getRunData,exchangeStep,getCityLight,rankList,userStepRecord,userRegister,getStepIndex']//校验登录状态
    ];

    /**
     * 1.通过encryptedData,iv,openid,session_key解密获取step步数
     * @return \think\response\Json
     */
    public function getRunData(){
        (new WxValidate())->goCheck('getrundata');//验证参数有效性
        $params=request()->post();
        $token=request()->header('token');
        //jwt解密
        try {
            $tokenarr = JwtUtil::jwtDecode($token);
            $unionid = $tokenarr->{'unionid'};
        } catch (Exception $e) {
            throw new ParameterException();
        }

        //返回sessen_key和openid
        $indexService=new StepService();
        $arr=$indexService->getRunData($params,$unionid);
        return json(['code'=>1,'data'=>$arr],200,$this->setHeader());
    }

    /**
     * 2.获取用户城市点亮数据,签到数据,总步数
     */
    public function getStepIndex(){

        (new WxValidate())->goCheck('getrundata');//验证参数有效性
        $params=request()->post();
        $token=request()->header('token');
        //jwt解密
        try {
            $tokenarr = JwtUtil::jwtDecode($token);
            $unionid = $tokenarr->{'unionid'};
        } catch (Exception $e) {
            throw new ParameterException();
        }
        $stepService=new StepService();

        //获取用户城市点亮数据,签到数据,总步数
        $data=$stepService->getCLAndSNAndRAndUDNAndStep($unionid,$params);
        return json($data,200,$this->setHeader());

    }

    /**
     * 单独获取用户城市点亮情况
     * @return \think\response\Json
     * @throws ParameterException
     */
    public function getStepCity(){
        $token=request()->header('token');
        //jwt解密
        try {
            $tokenarr = JwtUtil::jwtDecode($token);
            $unionid = $tokenarr->{'unionid'};
        } catch (Exception $e) {
            throw new ParameterException();
        }
        $stepService=new StepService();
        //获取用户城市点亮数据,签到数据,总步数
        $data=$stepService->getCityLight($unionid);
        return json($data,200,$this->setHeader());
    }


    /**
     * 3.用户提交累计步数
     */
    public function exchangeStep(){
        (new WxValidate())->goCheck('exchangeStep');//验证参数有效性
        $step=request()->post('step');
        if(intval($step)==0){
            throw new StepException();
        }
        $token=request()->header('token');
        //jwt解密
        try {
            $tokenarr = JwtUtil::jwtDecode($token);
            $unionid = $tokenarr->{'unionid'};
        } catch (Exception $e) {
            throw new ParameterException();
        }

        $stepService=new StepService();
        $stepService->exchangeStep(intval($step),$unionid);//用户提交累计步数

        //返回用户城市点亮数据,签到数据,总步数
        $data=$stepService->getCLAndSNAndRAndUDN($unionid);
        return json($data,200,$this->setHeader());

    }


    /**
     * 4.排行榜
     */
    public function rankList(){
        $token=request()->header('token');
        $page=empty(request()->post('page'))?1:intval(request()->post('page'));
        $size=empty(request()->post('size'))?100:intval(request()->post('size'));

        //jwt解密
        try {
            $tokenarr = JwtUtil::jwtDecode($token);
            $unionid = $tokenarr->{'unionid'};
        } catch (Exception $e) {
            throw new ParameterException();
        }

        //返回排行榜数据
        $stepService=new StepService();
        $data=$stepService->rankList($unionid,$page,$size);
        return json($data,200,$this->setHeader());

    }


    /**
     * 5.用户历史提交步数
     */
    public function userStepRecord(){
        $token=request()->header('token');
        //jwt解密
        try {
            $tokenarr = JwtUtil::jwtDecode($token);
            $unionid = $tokenarr->{'unionid'};
        } catch (Exception $e) {
            throw new ParameterException();
        }

        $stepService=new StepService();
        $data=$stepService->userStepRecord($unionid);
        return json($data,200,$this->setHeader());

    }

    /**
     * 用户签到
     */
    public function userRegister(){
        $token=request()->header('token');
        //jwt解密
        try {
            $tokenarr = JwtUtil::jwtDecode($token);
            $unionid = $tokenarr->{'unionid'};
        } catch (Exception $e) {
            throw new ParameterException();
        }

        $stepService=new StepService();
        $data=$stepService->userRegister($unionid);
        return json($data,200,$this->setHeader());

    }


}