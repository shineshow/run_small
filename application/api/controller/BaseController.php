<?php
/**
 * Created by PhpStorm.
 * User: Knight
 * Date: 2018/10/23
 * Time: 16:43
 */

namespace app\api\controller;


use think\Controller;
use app\api\service\Token as TokenService;

class BaseController extends Controller
{

    /**
     * 设置允许跨域
     */
    public function setHeader(){
        $host_name = isset($_SERVER['HTTP_ORIGIN']) ? $_SERVER['HTTP_ORIGIN'] : "*";
        return  [
            "Access-Control-Allow-Origin" => $host_name,
            "Access-Control-Allow-Credentials" => 'true',
            "Access-Control-Allow-Headers" => "x-token,x-uid,x-token-check,x-requested-with,content-type,Host,token"
        ];
    }

    /**
     * 验证是否是用户登录
     */
    protected function checkExclusiveScope()
    {
//        TokenService::needExclusiveScope();
    }


    /**
     * 验证并解析参数
     */
    protected function paramsValidateScope()
    {
//        ParamsService::paramsValidateScope();
    }


    /**
     * 验证token是否存在缓存中(用户登录验证)
     */
    protected function tokenValidateScope()
    {
        TokenService::tokenValidateScope();
    }
}