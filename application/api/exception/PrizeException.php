<?php
/**
 * Created by PhpStorm.
 * User: Knight
 * Date: 2018/10/23
 * Time: 16:46
 */

namespace app\api\exception;


class PrizeException extends BaseException
{
    //HTTP 状态码 404,200
    public $code=400;

    //错误具体信息
    public $msg='抽奖错误';

    //自定义的错误码
    public $errorCode=60000;
}