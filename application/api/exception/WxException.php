<?php
/**
 * Created by PhpStorm.
 * User: Knight
 * Date: 2018/10/23
 * Time: 16:46
 */

namespace app\api\exception;


class WxException extends BaseException
{
    //HTTP 状态码 404,200
    public $code=400;

    //错误具体信息
    public $msg='微信参数错误';

    //自定义的错误码
    public $errorCode=20000;
}