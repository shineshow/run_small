<?php
/**
 * Created by PhpStorm.
 * User: Knight
 * Date: 2018/3/23
 * Time: 16:09
 */

namespace app\api\util;


class RandStr
{
    static function GetRandStr($length){
        $str='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $str1='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789';
        $len=strlen($str)-1;
        $randstr=$str1[(mt_rand(0,strlen($str1)-1))];
        for($i=1;$i<$length;$i++){
            $num=mt_rand(0,$len);
            $randstr .= $str[$num];
        }
        return $randstr;
    }
}