<?php
/**
 * Created by PhpStorm.
 * User: Knight
 * Date: 2019/1/3
 * Time: 16:08
 */

namespace app\api\util;


use SoapClient;
use SoapFault;
use think\Log;

class SMS
{
    /**
     * @param $phone
     * @param $code
     * @return bool true:发送成功;false:发送失败
     */
    public static function sendSMS($phone,$code)
    {
        try {
            Log::save('--------发送短信---------');
            $client = new SoapClient("http://www.wemediacn.net/webservice/smsservice.asmx?WSDL");
            $client->soap_defencoding = 'utf-8';
            $client->decode_utf8 = false;
            $client->xml_encoding = 'utf-8';
            Log::save('手机号:'.$phone);
            Log::save('验证码:'.$code);
            $param = array(
                'mobile' => $phone,
                'FormatID' => 8,
                'Content' => '验证码:'.$code.'(15分钟内有效)，请勿泄露该验证码。',
                'ScheduleDate' => time(),
                'TokenID' => '7103038330612784');
            $sMsgID = $client->__Call("SendSMS", array($param));
//            var_dump($sMsgID);
            $sMsgID=json_encode($sMsgID);
            $sMsgID=json_decode($sMsgID,true);
            if ($sMsgID && substr($sMsgID['SendSMSResult'],0,3)=='OK:'){
//                echo 'Send successful!';
                Log::save('--------发送成功---------');
            }
            return true;
        } catch (SOAPFault $e) {
            Log::save($e);
            Log::save('--------发送失败---------');
            return false;
        }
    }
}