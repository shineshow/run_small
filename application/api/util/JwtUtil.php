<?php
/**
 * Created by PhpStorm.
 * User: Knight
 * Date: 2018/10/24
 * Time: 11:15
 */

namespace app\api\util;


use Firebase\JWT\JWT;

class JwtUtil
{
    private static  $key = "zd_small_FASDfasdasdasdwa2weqegq213saa24as";

    /**
     * jwt加密
     * @param $token
     * @return string
     */
    public static function jwtEncode($token){
        $jwt = JWT::encode($token, JwtUtil::$key,'HS256');
        return $jwt;
    }


    /**
     * jwt解密
     * @param $token 加密的值
     * @return mixed
     */
    public static function jwtDecode($jwt){

        try{
            //从jwt获取信息
            $decoded = JWT::decode($jwt, JwtUtil::$key, array('HS256'));
            return $decoded;
        }catch (\Exception $e){
            return [];
        }

    }



}