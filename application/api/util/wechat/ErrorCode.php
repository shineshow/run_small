<?php
/**
 * Created by PhpStorm.
 * User: Knight
 * Date: 2019/1/9
 * Time: 15:08
 */

namespace app\api\util\wechat;


class ErrorCode
{
    public static $OK = 0;
    public static $IllegalAesKey = -41001;
    public static $IllegalIv = -41002;
    public static $IllegalBuffer = -41003;
    public static $DecodeBase64Error = -41004;
}