<?php
/**
 * Created by PhpStorm.
 * User: Knight
 * Date: 2018/12/17
 * Time: 15:58
 */

namespace app\api\util;


class ArrayUtil
{

    /**
     * php除数组指定的key值(直接删除key值实现)
     * @param unknown $data
     * @param unknown $key
     * @return unknown
     */
    static function array_remove($data, $key){
        if(!array_key_exists($key, $data)){
            return $data;
        }
        $keys = array_keys($data);
        $index = array_search($key, $keys);
        if($index !== FALSE){
            array_splice($data, $index, 1);
        }
        return $data;

    }

    /**
     * php除数组指定的key值(通过直接重新组装一个数组)
     * @param unknown $data
     * @param unknown $key
     * @return unknown
     */
    static function array_remove1($data,$delKey)
    {
        $newArray = array();
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                if ($key !== $delKey) {
                    $newArray[$key] = $value;
                }
            }
        } else {
            $newArray = $data;
        }
        return $newArray;
    }
}