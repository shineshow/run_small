<?php
/**
 * Created by PhpStorm.
 * User: Knight
 * Date: 2018/10/23
 * Time: 17:02
 */

namespace app\api\util;


class WxApi
{
    /**
     * 小程序登录凭证校验。返回openid,unionid,session_key
     * @param $code
     */
    public function code2Session($code){
        $url="https://api.weixin.qq.com/sns/jscode2session?appid=".config('APPID')."&secret=".config('SECRET')."&js_code=".$code."&grant_type=authorization_code";
        $curlRequest=new CurlRequest();

        $jsonstr=$curlRequest->httpGet($url);
        return $jsonstr;
    }

}