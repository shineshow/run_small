<?php
/**
 * Created by PhpStorm.
 * User: Knight
 * Date: 2018/10/24
 * Time: 10:33
 */

namespace app\api\model;


use think\Model;

/**
 * 用户微信数据model
 * Class WxUser
 * @package app\api\model
 */
class WxUser extends Model
{

    protected $table='zd_wx_user';

}