<?php
/**
 * Created by PhpStorm.
 * User: Knight
 * Date: 2019/1/31
 * Time: 11:11
 */

namespace app\api\model;


use think\Model;

/**
 * 用户签到model
 * Class UserStep
 * @package app\api\model
 */
class UserRegister extends Model
{
    protected $table='zd_user_register';
}