<?php
/**
 * Created by PhpStorm.
 * User: Knight
 * Date: 2018/11/1
 * Time: 13:37
 */

namespace app\api\model\nav_index;


use think\Model;

/**
 * 首页底部菜单一级
 * Class NavigationFirst
 * @package app\api\model\nav_index
 */
class NavigationFirst extends Model
{
    protected $table = 'zd_navigation_index';

    protected $hidden=['id','pid','status'];

    public function navigation()
    {
        return $this->hasMany('NavigationSecond', 'pid', 'id');
    }

}