<?php
/**
 * Created by PhpStorm.
 * User: Knight
 * Date: 2019/1/31
 * Time: 10:21
 */

namespace app\api\model;


use think\Model;

/**
 * 城市步数配置model
 * Class StepCity
 * @package app\api\model
 */
class StepCity extends Model
{
    protected $table = 'zd_step_city';
}