<?php
/**
 * Created by PhpStorm.
 * User: Knight
 * Date: 2019/1/30
 * Time: 14:50
 */

namespace app\api\model;


use think\Model;

/**
 * 首页模块按钮model
 * Class ZdIndexButton
 * @package app\api\model
 */
class ZdIndexButton extends Model
{
    protected $table='zd_index_button';
}