<?php
/**
 * Created by PhpStorm.
 * User: Knight
 * Date: 2019/1/31
 * Time: 10:21
 */

namespace app\api\model;


use think\Model;

/**
 * 用户城市点亮model
 * Class UserStepCity
 * @package app\api\model
 */
class UserStepCity extends Model
{
    protected $table='zd_step_city_user';
}