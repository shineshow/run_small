<?php
/**
 * Created by PhpStorm.
 * User: Knight
 * Date: 2019/1/31
 * Time: 12:23
 */

namespace app\api\model;


use think\Model;

/**
 * 用户抽奖次数
 * Class UserDrawConf
 * @package app\api\model
 */
class UserDrawConf extends  Model
{
    protected $table='zd_user_draw_conf';
}