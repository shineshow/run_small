<?php
/**
 * Created by PhpStorm.
 * User: Knight
 * Date: 2019/1/31
 * Time: 11:11
 */

namespace app\api\model;


use think\Model;

/**
 * 用户步数model
 * Class UserStep
 * @package app\api\model
 */
class UserStep extends Model
{
    protected $table='zd_user_step';

    /**
     * 关联用户步数
     * @return \think\model\relation\BelongsTo
     */
    public function userStepCity(){
        return $this->belongsTo('UserStepCity','unionid','unionid');
    }

    /**
     * 关联签到
     * @return \think\model\relation\BelongsTo
     */
    public function userRegister(){
        return $this->belongsTo('UserRegister','unionid','unionid');
    }


    /**
     * 用户抽奖数据
     */
    public function userDrawConf(){
        return $this->belongsTo('UserDrawConf','unionid','unionid');
    }

    /**
     * 关联微信用户数据
     * @return \think\model\relation\BelongsTo
     */
    public function wxUser(){
        return $this->belongsTo('WxUser','unionid','unionid');
    }


}