<?php
/**
 * Created by PhpStorm.
 * User: Knight
 * Date: 2019/2/1
 * Time: 10:47
 */

namespace app\api\model;


use think\Model;

/**
 * 用户步数日志
 * Class UserStepRecord
 * @package app\api\model
 */
class UserStepRecord extends Model
{
    protected $table='zd_step_recored';
}