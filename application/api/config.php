<?php
/**
 * Created by PhpStorm.
 * User: Knight
 * Date: 2018/10/23
 * Time: 16:39
 */

return [
    'APPID'=>'wx64e0067dcd57d5e8',
    'SECRET'=>'e5ac12f8c1bd129a27e94d94ba47fe75',


    'uri'=>'https://wx.helloshineshow.com',

    // 异常处理handle类 留空使用 \think\exception\Handle
    'exception_handle'       => 'app\api\exception\ExceptionHandler',


    //设置redis缓存
    'redis' => [
        'host'       => '127.0.0.1',
        'port'       => 6379,
        'password'   => '123456',
        'select'     => 3,
        'timeout'    => 0,
        'expire'     => 0,
        'persistent' => false,
        'prefix'     => '',
        'userCacheTime'=>3000, //用户登录过期时间
        'refurbishTime'=>300, //过期前的最大刷新时间
        'pickStarTime'=>600, //兑星数据过期时间
    ],


    "step"=>[
        "longStep"=>20000,//每日最高步数限制
    ],

    "city"=>[
        0=>"香港",
        1=>"越南",
        2=>"新加坡",
        3=>"马来西亚",
        4=>"印度尼西亚",
        5=>"孟加拉",
        6=>"巴基斯坦",
        7=>"阿联酋",
        8=>"肯尼亚",
        9=>"北京",
    ]

];