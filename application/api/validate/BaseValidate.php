<?php
/**
 * Created by PhpStorm.
 * User: Knight
 * Date: 2018/10/23
 * Time: 16:55
 */

namespace app\api\validate;


use app\api\exception\ParameterException;
use think\Request;
use think\Validate;

class BaseValidate extends Validate
{
    public function goCheck($sce){
        //获取http传入的参数
        //对这些参数校验
        $params=Request::instance()->param();
        $result=$this->scene($sce)->batch()->check($params);
        if(!$result){
            $e=new ParameterException([
                'msg'=>array_values($this->error)[0],
            ]);
//            $e=new ParameterException([
//                'msg'=>$this->error,
//            ]);
            throw $e;
        }else{
            return true;
        }
    }

}