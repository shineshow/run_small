<?php
/**
 * Created by PhpStorm.
 * User: Knight
 * Date: 2018/10/23
 * Time: 16:57
 */

namespace app\api\validate;


class WxValidate extends BaseValidate
{
    protected $rule = [
        'code' => 'require',
        'encrypteddata'=>'require',
        'iv'=>'require',
        'sessionkey'=>'require',
        'step'=>'require',
    ];


    protected $scene = [
        'index' => ['code'],
        'gettoken' => ['encrypteddata','iv','sessionkey'],
        'getrundata'=>['encrypteddata','iv','sessionkey'],
        'exchangeStep'=>['step'],
    ];


}