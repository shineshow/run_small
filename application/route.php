<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

return [
    '__pattern__' => [
        'name' => '\w+',
    ],
    '[hello]'     => [
        ':id'   => ['index/hello', ['method' => 'get'], ['id' => '\d+']],
        ':name' => ['index/hello', ['method' => 'post']],
    ],


    '[api]'=>[
        'index/codetoopneid'=>['api/Index/codeToOpneid',['method'=>'post']],
        'index/gettoken'=>['api/Index/getToken',['method'=>'post']],


        'indexbutton/getbutton'=>['api/IndexButton/getButton',['method'=>'post']],
        'indexbutton/getbottom'=>['api/IndexButton/getBottomButton',['method'=>'post']],


        'step/getrundata'=>['api/Step/getRunData',['method'=>'post']],
        'step/getstepindex'=>['api/Step/getStepIndex',['method'=>'post']],
        'step/getstepcity'=>['api/Step/getStepCity',['method'=>'post']],
        'step/exchangestep'=>['api/Step/exchangeStep',['method'=>'post']],
        'step/ranklist'=>['api/Step/rankList',['method'=>'post']],
        'step/getstepindex'=>['api/Step/getStepIndex',['method'=>'post']],
        'step/usersteprecord'=>['api/Step/userStepRecord',['method'=>'post']],
        'step/userregister'=>['api/Step/userRegister',['method'=>'post']],

        'prize/draw'=>['api/Prize/draw',['method'=>'post']],
        'prize/checkprize'=>['api/Prize/checkprize',['method'=>'post']],
        'prize/getuserprize'=>['api/Prize/getUserprize',['method'=>'post']],
        'prize/getprizelist'=>['api/Prize/getPrizeList',['method'=>'post']],
    ],



];
