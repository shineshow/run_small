<?php
/**
 * tpAdmin [a web admin based ThinkPHP5]
 *
 * @author    yuan1994 <tianpian0805@gmail.com>
 * @link      http://tpadmin.yuan1994.com/
 * @copyright 2016 yuan1994 all rights reserved.
 * @license   http://www.apache.org/licenses/LICENSE-2.0
 */

return [
    // 网站信息
    'name'        => '跑步小程序平台',
    'title'       => '跑步小程序平台',
    'version'     => 'v1.0',
    'keywords'    => '跑步小程序平台',
    'description' => '跑步小程序平台',

    // 登录地址
    'logout_url'        => 'Pub/logout',
    'login_frame'       => 'Pub/loginFrame',
];