<?php
/**
 * tpAdmin [a web admin based ThinkPHP5]
 *
 * @author    yuan1994 <tianpian0805@gmail.com>
 * @link      http://tpadmin.yuan1994.com/
 * @copyright 2016 yuan1994 all rights reserved.
 * @license   http://www.apache.org/licenses/LICENSE-2.0
 */

use \think\Request;

$basename = Request::instance()->root();
if (pathinfo($basename, PATHINFO_EXTENSION) == 'php') {
    $basename = dirname($basename);
}

return [
    // 模板参数替换
    'view_replace_str' => [
        '__ROOT__'   => $basename,
        '__STATIC__' => $basename . '/static/admin',
        '__LIB__'    => $basename . '/static/admin/lib',
    ],

    // traits 目录
    'traits_path'      => APP_PATH . 'admin' . DS . 'traits' . DS,

    // 异常处理 handle 类 留空使用 \think\exception\Handle
    'exception_handle' => '\\TpException',

    'template' => [
        // 模板引擎类型 支持 php think 支持扩展
        'type'            => 'Think',
        // 模板路径
        'view_path'       => '',
        // 模板后缀
        'view_suffix'     => '.html',
        // 预先加载的标签库
        'taglib_pre_load' => 'app\\admin\\taglib\\Tp',
        // 默认主题
        'default_theme'   => '',
    ],

    'ftp_config' => [
        //ftp(外网服务器)上传文件相关参数
        'FTP_SEVER'       => '182.254.209.213',  //此地址，作为图片读取的位置 请上线前仔细确认

        'FTP_NAME'       => 'breakdingd',//ftp帐户
        'FTP_PWD'        => '201314sxfi',//ftp密码
        'FTP_PORT'       => '21',//ftp端口,默认为21
        'FTP_PASV'       => true,//是否开启被动模式,true开启,默认不开启
        'FTP_SSL'        => false,//ssl连接,默认不开启
        'FTP_TIMEOUT'    => 60,//超时时间,默认60,单位 s
        'FILE_ROOT'    => '/webActivity/wx-web/img/',//图片服务器根目录
    ]
];
