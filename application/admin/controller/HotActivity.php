<?php
/**
 * Created by PhpStorm.
 * User: lenovo
 * Date: 2018-11-01
 * Time: 13:06
 */

namespace app\admin\controller;

use app\admin\Controller;
use think\Request;
use app\admin\model\HotActivity as HotActivityModel;

class HotActivity extends Controller
{

    use \app\admin\traits\controller\Controller;

    protected static $isdelete = false;

    protected static $blacklist = [];

    protected function filter(&$map)
    {
        if ($this->request->param("title")) {
            $map['title'] = ["like", "%" . $this->request->param("title") . "%"];
        }
        if ($this->request->param("a_city")) {
            $map['a_city'] = ["like", "%" . $this->request->param("a_city") . "%"];
        }
        if ($this->request->param("a_year")) {
            $map['a_year'] = ["like", "%" . $this->request->param("a_year") . "%"];
        }
        if ($this->request->param("a_month")) {
            $map['a_month'] = ["like", "%" . $this->request->param("a_month") . "%"];
        }
        if ($this->request->param("a_day")) {
            $map['a_day'] = ["like", "%" . $this->request->param("a_day") . "%"];
        }
    }

    // Controller 默认方法(index)
    public function index(){

        $model = new HotActivityModel();

        // 列表过滤器，生成查询Map对象
        $map = $this->search($model, [$this->fieldIsDelete => $this::$isdelete]);

        // 特殊过滤器，后缀是方法名的
        $actionFilter = 'filter' . $this->request->action();
        if (method_exists($this, $actionFilter)) {
            $this->$actionFilter($map);
        }

        // 自定义过滤器
        if (method_exists($this, 'filter')) {
            $this->filter($map);
        }

        $this->datalist($model, $map);

        return $this->view->fetch();
    }

    /**
     * 编辑 & 新增
     * @return mixed
     */
    public function edit() {
        $id = Request::instance()->param('id');

        if (false === $model = HotActivityModel::getById($id)) {
            return ajax_return_adv_error('系统未找到ID为' . $id . '的记录');
        }

        $this->view->assign('model', $model);
        return $this->view->fetch();
    }

    public function update()
    {
        $id = Request::instance()->post('id/d');
        // 获取传入的 首页图片ID
        $model = HotActivityModel::getById($id);
        if (is_null($model)) {
            $model = new  HotActivityModel();
        }

        // 数据更新
        $startTimes = Request::instance()->post('start_time');
        $model->a_city = Request::instance()->post('a_city');
        $model->start_time = $startTimes;
        $model->a_top = Request::instance()->post('a_top');
        $model->title = Request::instance()->post('title');
        $model->activity_code = Request::instance()->post('activity_code');
        $model->detail_url = Request::instance()->post('detail_url');
        $model->url = Request::instance()->post('url');
        $model->activity_pto = Request::instance()->post('imgurl');
        $model->activity_adress = Request::instance()->post('activity_adress');
        $model->activity_deadline = Request::instance()->post('activity_deadline');
        $start = explode("-", $startTimes);
        $model->activity_countimes = $start[0].'.'.$start[1];
        $model->isindex = Request::instance()->post('activity_deadline');
//        $model->a_city = Request::instance()->post('a_city');
//        $model->a_city = $model;
//        Db::startTrans();
//        try {
////            $model_object = new  HotActivityModel();
//            $ret = Db::table("zd_hot_activity")->insert($model);
//            // 提交事务
//            Db::commit();
//        } catch (\Exception $e) {
//            // 回滚事务
//            Db::rollback();
//
//            return ajax_return_adv_error($e->getMessage());
//        }

        if (!$model->validate()->save()) {
            if(is_null($model->getError())){
                return ajax_return_adv("操作成功");
            }else{
                return ajax_return_adv_error('更新错误：' . $model->getError());
            }
        } else {
            return ajax_return_adv("操作成功");
        }
    }

    // 文件上传预留
    public function upload()
    {
        // 获取上传文件
        $file = request() -> file('activity_pto');

        // 验证图片,并移动图片到框架目录下。E:\Apache24\htdocs\webActivity\wx-web\img
        $info = $file -> validate(['size' => 512000,'ext' => 'jpg,png,jpeg','type' => 'image/jpeg,image/png']);

        if($info){
            // $info->getExtension();         // 文件扩展名
            // 获取得到图片的绝对路径 => E:\xampp1\htdocs\WX_SMALL\public\uploads\20181116\*.jpg
            $mes = $info->getPathname();      // 文件名
            $mes = substr($mes,16);
            $domain=Request::instance()->domain();
            $domain=str_replace('http','https',$domain);
            $mes = $domain.$mes;
            return $mes;
        }else{
            // 文件上传失败后的错误信息
            $mes = $file->getError();
            return ajax_return_adv_error($mes);
        }
    }

    // 连接FTP服务器(上传图片功能)
    public function ftpConnect(){
        $connect = ftp_connect();
        ftp_login($connect,'breakdingd','201314sxfi');

        ftp_put($connect,"1111111.doc","D:/song/ftpdoc.doc",FTP_BINARY);

        //ftp_put($conn,"上传后文件的命名.doc","指定本地要上的文件"，传输模式) *FTP_ASCII   FTP_BINARY（文件中文内容不会乱码）
		ftp_close($connect);
    }
}