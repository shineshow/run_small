<?php
/**
 * Created by PhpStorm.
 * User: lenovo
 * Date: 2018-12-17
 * Time: 10:56
 */

namespace app\admin\controller;

use PHPMailer\PHPMailer\PHPMailer;
use think\Controller;


class EmailController extends Controller
{
    public function phpinfos(){
        echo phpinfo();
    }

    public static function sends_SSLMail(){
        echo phpinfo();
        try {
            $thinkEmail_Conf = config("THINK_EMAIL");

            $mail = new PHPMailer();
            $mail->isSMTP();// 使用SMTP协议发送邮件
            $mail->SMTPSecure = 'ssl';//  使用SSL加密协议发送邮件
            $mail->SMTPAuth = true; // SSL 协议设置

            $mail->Host = $thinkEmail_Conf["SMTP_HOST"]; //  邮箱服务器
            $mail->Port = $thinkEmail_Conf["SMTP_PORT"]; //  邮箱端口号
            $mail->Username = $thinkEmail_Conf["SMTP_USER"]; //  邮箱账号
            $mail->Password = $thinkEmail_Conf["SMTP_PASS"]; //  邮箱授权码

            $mail->CharSet  = "UTF-8";  //字符集
            $mail->Encoding = "base64"; //编码方式

            $mail->From = $thinkEmail_Conf["SMTP_USER"];
            $mail->FromName = "Ding";
            $mail->addAddress($thinkEmail_Conf["SMTP_USER"], 'Recipients Name');/*Add a recipient*/
            $mail->addReplyTo($thinkEmail_Conf["SMTP_USER"], "Ding");
            //$mail->WordWrap = 70;/*DEFAULT = Set word wrap to 50 characters*/
            $mail->isHTML(true);/*Set email format to HTML (default = true)*/
            $mail->Subject = "库存不足";
            $mail->Body    = "礼品库存不足";
            $mail->AltBody = "礼品库存不足";
            var_dump($mail);
            if(!$mail->send()) {
                echo 'Message could not be sent.';
                echo $mail->ErrorInfo;
            } else {
                echo "YES";
                header("Location: ../docs/confirmSubmit.html");
            }
        }catch (\Exception $exception){
            return $exception->getMessage();
        }
    }

    public static function sends_NOSSLMail(){
        try {
            $thinkEmail_Conf = config("THINK_EMAIL");

            $mail = new PHPMailer();
            $mail->isSMTP();// 使用SMTP协议发送邮件
            $mail->setSMTPInstance($mail->getSMTPInstance());
            $mail->SMTPAuth = true;
            $mail->SMTPSecure = false;

            $mail->Host = "smtp.163.com"; //  邮箱服务器
            $mail->Port = 25;
            $mail->Username = "breakding7657@163.com"; //  邮箱账号
            $mail->Password = "breakding7657"; //  邮箱授权码

            $mail->CharSet  = "UTF-8";  //字符集
            $mail->Encoding = "base64"; //编码方式

            $mail->From = $thinkEmail_Conf["SMTP_USER"];
            $mail->FromName = "Ding";
            $mail->addAddress($thinkEmail_Conf["SMTP_USER"], 'Recipients Name');/*Add a recipient*/
            $mail->addReplyTo($thinkEmail_Conf["SMTP_USER"], "Ding");
            //$mail->WordWrap = 70;/*DEFAULT = Set word wrap to 50 characters*/
            $mail->isHTML(true);/*Set email format to HTML (default = true)*/
            $mail->Subject = "库存不足";
            $mail->Body    = "礼品库存不足";
            $mail->AltBody = "礼品库存不足";
//            var_dump($mail);
            if(!$mail->send()) {
                echo $mail->ErrorInfo;
            } else {
                echo "YES";
                header("Location: ../docs/confirmSubmit.html");
            }
        }catch (\Exception $exception){
            return $exception->getMessage();
        }
    }

}