<?php
/**
 * Created by PhpStorm.
 * User: lenovo
 * Date: 2018-10-30
 * Time: 16:03
 */

namespace app\admin\controller;


use app\admin\Controller;
use think\Request;
use app\admin\model\GraphicText as GraphicTextModel;

class GraphicText extends  Controller
{

    use \app\admin\traits\controller\Controller;

    protected static $isdelete = false;

    protected static $blacklist = [];

    protected function filter(&$map)
    {
        if ($this->request->param("title")) {
            $map['title'] = ["like", "%" . $this->request->param("title") . "%"];
        }
        if ($this->request->param("isindex")) {
            $map['isindex'] = ["like", "%" . $this->request->param("isindex") . "%"];
        }
    }

     // Controller 默认方法(index)
     public function index(){

         $model = new GraphicTextModel();

         // 列表过滤器，生成查询Map对象
         $map = $this->search($model, [$this->fieldIsDelete => $this::$isdelete]);

         // 特殊过滤器，后缀是方法名的
         $actionFilter = 'filter' . $this->request->action();
         if (method_exists($this, $actionFilter)) {
             $this->$actionFilter($map);
         }

         // 自定义过滤器
         if (method_exists($this, 'filter')) {
             $this->filter($map);
         }
         $map['_order_by']='weight desc';
         $this->datalist($model, $map);

         return $this->view->fetch();
    }

    /**
     * 编辑 & 新增
     * @return mixed
     */
    public function edit() {
        $id = Request::instance()->param('id');

        if (false === $graphicTextModel = GraphicTextModel::getById($id)) {
            return ajax_return_adv_error('系统未找到ID为' . $id . '的记录');
        }

        $this->view->assign('graphicTextModel', $graphicTextModel);
        return $this->view->fetch();
    }

    public function update()
    {
        $id = Request::instance()->post('id/d');
        // 获取传入的 首页图片ID
        $graphicTextModel_object = GraphicTextModel::getById($id);
        if (is_null($graphicTextModel_object)) {
            $graphicTextModel_object = new  GraphicTextModel();
            $graphicTextModel_object->create_times = date("Y-m-d H:i:s");
        }

        // 数据更新
        $graphicTextModel_object->photo_exhibition = Request::instance()->post('imgurl');
        $graphicTextModel_object->title = htmlspecialchars_decode(Request::instance()->post('title'));
        $graphicTextModel_object->url = htmlspecialchars_decode(Request::instance()->post('url'));
        $graphicTextModel_object->isindex = Request::instance()->post('isindex');
        $graphicTextModel_object->weight = Request::instance()->post('weight');

        if (!$graphicTextModel_object->validate()->save()) {
            if(is_null($graphicTextModel_object->getError())){
                return ajax_return_adv("操作成功");
            }else{
                return ajax_return_adv_error('更新错误：' . $graphicTextModel_object->getError());
            }
        } else {
            return ajax_return_adv("操作成功");
        }
    }

    // 文件上传预留
    public function upload()
    {
        // 获取上传文件
        $file = request() -> file('photo_exhibition');

        // 验证图片,并移动图片到框架目录下。
        $info = $file -> validate(['size' => 512000,'ext' => 'jpg,png,jpeg','type' => 'image/jpeg,image/png']) -> move(ROOT_PATH.'public'.DS.'uploads');

        if($info){
            // $info->getExtension();         // 文件扩展名
            // 获取得到图片的绝对路径 => E:\xampp1\htdocs\WX_SMALL\public\uploads\20181116\*.jpg
            $mes = $info->getSaveName();     // 文件名相对路径
            $mes = str_replace('\\','/',$mes);
            $domain = Request::instance()->domain();
            $domain = str_replace('http', 'https', $domain);
            $mes = $domain .'/WX_SMALL/public/uploads/'. $mes;
            return $mes;
        }else{
            // 文件上传失败后的错误信息
            $mes = $file->getError();
            return ajax_return_adv_error($mes);
        }
    }

}