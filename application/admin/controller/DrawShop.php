<?php
/**
 * Created by PhpStorm.
 * User: lenovo
 * Date: 2018-11-01
 * Time: 13:06
 */

namespace app\admin\controller;

use app\admin\Controller;
use think\Request;
use app\admin\model\DrawShop as DrawShopModel;

class DrawShop extends Controller
{

    use \app\admin\traits\controller\Controller;

    protected static $isdelete = false;

    protected static $blacklist = [];

    protected function filter(&$map)
    {
        if ($this->request->param("name")) {
            $map['name'] = ["like", "%" . $this->request->param("name") . "%"];
        }
        if ($this->request->param("source")) {
            $map['source'] = ["like", "%" . $this->request->param("source") . "%"];
        }
        if ($this->request->param("prize_type")) {
            $map['prize_type'] = ["like", "%" . $this->request->param("prize_type") . "%"];
        }
        if ($this->request->param("isindex")) {
            $map['isindex'] = ["like", "%" . $this->request->param("isindex") . "%"];
        }
    }

    // Controller 默认方法(index)
    public function index(){

        $model = new DrawShopModel();

        // 列表过滤器，生成查询Map对象
        $map = $this->search($model, [$this->fieldIsDelete => $this::$isdelete]);

        // 特殊过滤器，后缀是方法名的
        $actionFilter = 'filter' . $this->request->action();
        if (method_exists($this, $actionFilter)) {
            $this->$actionFilter($map);
        }

        // 自定义过滤器
        if (method_exists($this, 'filter')) {
            $this->filter($map);
        }

        $this->datalist($model, $map);

        return $this->view->fetch();
    }

    /**
     * 编辑 & 新增
     * @return mixed
     */
    public function edit() {
        $id = Request::instance()->param('id');

        if (false === $model = DrawShopModel::getById($id)) {
            return ajax_return_adv_error('系统未找到ID为' . $id . '的记录');
        }

        $this->view->assign('model', $model);
        return $this->view->fetch();
    }

    public function update()
    {
        $id = Request::instance()->post('id');
        // 获取传入的 首页图片ID
        if(!is_numeric(Request::instance()->post('number'))){
            return ajax_return_adv_error('请确认是否有效库存');
        }
        $model = DrawShopModel::getById($id);
        if (is_null($model)) {
            $model = new  DrawShopModel();
        }

        // 数据更新
        $model->image = Request::instance()->post('imgurl');
        $model->name =Request::instance()->post('name');
        $model->source =Request::instance()->post('source');
        $model->number =Request::instance()->post('number');
        $model->chance =Request::instance()->post('chance');
        $model->prize_type =Request::instance()->post('prize_type');
        $model->isindex =Request::instance()->post('isindex');

        if (!$model->validate()->save()) {
            if(is_null($model->getError())){
                return ajax_return_adv("操作成功");
            }else{
                return ajax_return_adv_error('更新错误：' . $model->getError());
            }
        } else {
            return ajax_return_adv("操作成功");
        }
    }

    // 文件上传预留
    public function upload()
    {
        // 获取上传文件
        $file = request() -> file('image');

        // 验证图片,并移动图片到框架目录下。
        $info = $file -> validate(['size' => 512000,'ext' => 'jpg,png,jpeg','type' => 'image/jpeg,image/png']) -> move(ROOT_PATH.'public'.DS.'uploads');

        if($info){
            // $info->getExtension();         // 文件扩展名
            // 获取得到图片的绝对路径 => E:\xampp1\htdocs\WX_SMALL\public\uploads\20181116\*.jpg
            $mes = $info->getPathname();      // 文件名
            $mes = substr($mes,16);
            $mes = 'http://192.168.2.231'.$mes;
            return $mes;
        }else{
            // 文件上传失败后的错误信息
            $mes = $file->getError();
            return ajax_return_adv_error($mes);
        }
    }
}